package org.beetl.sql.springboot.simple;

import lombok.Data;
import org.beetl.sql.annotation.entity.Table;
import org.beetl.sql.mapper.BaseMapper;


public interface DepartmentMapper  extends BaseMapper<Department> {

}
